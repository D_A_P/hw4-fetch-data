const API_URL = "https://ajax.test-danit.com/api/swapi/films"; 
const filmList = document.getElementById('filmList');

function getFilmList() {
    fetch(API_URL, {
        method: 'GET'           
    })
    .then((response) => response.json())
    .then((films) => {
        films.forEach( film => {
            const filmElement = document.createElement('li');
            filmElement.innerHTML = `
            <h2>${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <div id="characters-${film.episodeId}"></div>
        `;

            filmList.append(filmElement);
            charactersFilm(film.episodeId, film.characters);
               
        });
    })
};

function charactersFilm(episodeId, characters) {
    const charactersContainer = document.getElementById(`characters-${episodeId}`);
    characters.forEach(url => {
        fetch(url, {
           method: 'GET'            
       })
            .then((response) => response.json()) 
            .then(character => {                
                const characterItem = document.createElement('div');
                characterItem.classList = 'characterItem';
                characterItem.innerHTML = `${character.name}`;
                charactersContainer.append(characterItem);
            })
    })
}  
getFilmList();


